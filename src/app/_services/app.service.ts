import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class AppService {

  private itemsSource = new BehaviorSubject(0);
  items$ = this.itemsSource.asObservable();

  constructor(private http: Http) { }

  public loadLocal() {
    return this.http.request('../assets/items.json')
         .map(res => res.json());
  }

  public setItems(data){
    this.itemsSource.next(data);
  }

}
