import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class CartService {
  private cartCounterSource = new BehaviorSubject<number>(0);
  cartCounter$ = this.cartCounterSource.asObservable();
  private cartPriceSource = new BehaviorSubject<number>(0);
  cartPriceCounter$ = this.cartPriceSource.asObservable();
  private itemsCartSource = new BehaviorSubject([]);
  itemsCartS$ = this.itemsCartSource.asObservable();
  constructor() { }

  public countUp() {
    let n = this.cartCounterSource.getValue();
    n++;
    this.cartCounterSource.next(n);
  }

  public countDown() {
    let n = this.cartCounterSource.getValue();
    n--;
    this.cartCounterSource.next(n);
  }

  public add(itemP){
    const items = this.itemsCartSource.getValue();
    const price = this.cartPriceSource.getValue()+itemP.price;
    this.cartPriceSource.next(price);
    for(let i = 0; i<items.length; i++){
      const item = items[i].item;
      if(item._id === itemP._id)
      {
        items[i].num ++;
        this.itemsCartSource.next(items);
        return;
      }
    }
    items.push({'item': itemP, 'num': 1});
    this.itemsCartSource.next(items);
    this.countUp();
  }

  public remove(i, items){
    const n = items[i].num;
    const price = this.cartPriceSource.getValue()-items[i].item.price;
    this.cartPriceSource.next(price);
    if(n>1)  {
      items[i].num--;
    }
    else {
      items.splice(i,1);
      this.countDown();
    }
    this.itemsCartSource.next(items);
  }
}
