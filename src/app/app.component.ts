import { Component, OnInit, AfterViewInit, ElementRef,
    ViewChild} from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { AppService } from './_services/app.service';

@Component({
  selector: 'app-root',
  animations: [
   trigger(
     'enterAnimation', [
       transition(':enter', [
         style({transform: 'translateX(100%)', opacity: 0}),
         animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
       ]),
       transition(':leave', [
         style({transform: 'translateX(0)', opacity: 1}),
         animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
       ])
     ]
   )
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild('containerLoad') elementView: ElementRef;
  private items;
  private loaded: boolean;
  private height: number;
  private margin: number;
  private loadingJSON: boolean;

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.loaded = false;
    this.height = window.innerHeight;
    this.margin = 0;
    this.loadingJSON = false;
    this.loadLocal();
  }

  ngAfterViewInit() {
  }

  ver(){
    console.log('atras');
  }

  private loadJSON(){
    this.loadingJSON = true;
  }

  private closeLoading(){
    this.loadingJSON = false;
  }

  private loadFile(event) {
   const reader = new FileReader();
     if (event.target.files && event.target.files.length > 0) {
       const file: File = event.target.files[0];
       reader.readAsText(file);
       reader.onload = () => {
          // The file's text will be printed here
          console.log(reader.result);
          try {
            JSON.parse(reader.result);
          } catch (e) {
            console.log(e);
          }
        };
     }
   }

  private loadFromUrl() {
  }

  private loadLocal() {
    this.appService.loadLocal()
    .subscribe(
      data=> {
        const n = data.map(i => {
          return {'item': i, 'cartAmnt': 0};
        });
        this.appService.setItems(data);
      }
    );
    this.loadingJSON = false;
    this.loaded = true;
  }

  private removeItems(){
    this.appService.setItems([]);
    this.loaded = false;
  }
}
