import { Component, OnInit,
  EventEmitter, Input, Output } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { CartService } from '../_services/index';

@Component({
  selector: 'app-cart',
  animations: [
   trigger(
     'enterAnimation', [
       transition(':leave', [
         style({transform: 'translateX(0)', opacity: 1}),
         animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
       ])
     ]
   )
  ],
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  @Output() onClose = new EventEmitter<boolean>();
  private items: Array<any>;
  private total: number;

  constructor(
    private cartService: CartService) {
    this.cartService.itemsCartS$.subscribe(next => {
      this.items = next;
    });

    this.cartService.cartPriceCounter$.subscribe(next => {
      this.total = next;
    });
  }

  ngOnInit() {
  }

  private closeCart(){
    this.onClose.emit(true);
  }

  private add(item)
  {
    this.cartService.add(item);
  }

  private remove(i)
  {
    this.cartService.remove(i, this.items);
  }

}
