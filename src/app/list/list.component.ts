import { Component, OnInit, Input } from '@angular/core';
import { CartService, AppService } from '../_services/index';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  private items;

  constructor(
    private cartService: CartService,
    private appService: AppService
  ) {
    this.appService.items$.subscribe(v => {
      this.items = v;
    });
  }

  ngOnInit() {
  }

  private addToCart(item) {
    this.cartService.add(item);
  }
}
