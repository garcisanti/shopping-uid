import { Component, OnInit, Renderer2, ElementRef } from '@angular/core';
import { CartService } from '../_services/cart.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  num= 0;
  private cartAnimation = false;
  private openedCart: boolean;

  constructor(private cartService: CartService,
    private renderer: Renderer2,
    private el: ElementRef) {
    this.cartService.cartCounter$.subscribe(v => {
      this.num = v;
      this.cartAnimation = true;
      setTimeout(()=>{
        this.cartAnimation = false;
      },500);
    });
  }

  ngOnInit() {
    this.openedCart = false;
  }

  private openCart(){
    this.renderer.addClass(document.body, 'hide-overflow');
            // window.scrollTo(0, 0);
    this.openedCart = true;
  }

  private onClose(close: boolean){
    this.renderer.removeClass(document.body, 'hide-overflow');
    this.openedCart = !close;
  }


}
