[
  '{{repeat(20)}}',
  {
    _id: '{{objectId()}}',
    index: '{{index()}}',
    name: '{{lorem(integer(1, 3), "words")}}',
    specs: '{{lorem(1,"sentences")}}',
    category: '{{random("fruits", "drinks", "health", "snacks")}}',
    price: '{{integer(2000, 50000)}}',
    quantity: '{{integer(0, 12)}}',
    sold: '{{integer(0, 30)}}'
  }
]
